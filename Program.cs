﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CongHaiSoLon
{
    class Program
    {
        static int[] LayDaySo(string pDaySo, int maxLength)
        {
            int[] daySo = new int[maxLength];
            for (int i = 0; i < pDaySo.Length; i++)
            {
                daySo[maxLength - pDaySo.Length + i] = int.Parse(pDaySo[i].ToString());
            }
            return daySo;
        }

        static void Main(string[] args)
        {
            int[] sum = Sum();
            // Hiển thị kết quả
            Console.WriteLine(string.Format("Ket qua : {0}", string.Join(string.Empty, sum)));
            Console.ReadLine();

        }
        static public int[] Sum()
        {
            // Nhập 2 số lớn từ bàn phím.
            Console.WriteLine("Nhap so thu 1 : ");
            string soLonThuNhat = Console.ReadLine();
            Console.WriteLine("Nhap so thu 2 : ");
            string soLonThuHai = Console.ReadLine();

            // Thực hiện cộng bằng mảng
            // Lấy maxLength là chiều dài của chuỗi có nhiều ký tự nhất
            int maxLength = soLonThuNhat.Length > soLonThuHai.Length ? soLonThuNhat.Length + 1 : soLonThuHai.Length + 1;
            // Chuyển 2 dãy số đầu vào thành 2 chuỗi số
            int[] daychuSoThuNhat = LayDaySo(soLonThuNhat, maxLength);
            int[] daychuSoThuHai = LayDaySo(soLonThuHai, maxLength);
            // Khai báo chuỗi kết quả đầu ra
            int[] daychuSoKetQua = new int[maxLength];

            int nho = 0;// lưu lại giá trị nhớ cho lần cộng sau
            int kq = 0;// lưu lại kết quả của từng lận cộng
            for (int i = maxLength - 1; i > -1; i--)
            {
                kq = daychuSoThuNhat[i] + daychuSoThuHai[i] + nho;
                daychuSoKetQua[i] = kq % 10;
                nho = kq / 10;
            }
            return daychuSoKetQua;
        }
    }
}
